package br.com.jera.retro.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by atomicavocado on 2/24/16.
 */
public class Task implements Serializable {

    @SerializedName("id")
    public int id;
    @SerializedName("sender")
    public String sender;
    @SerializedName("description")
    public String description;
    @SerializedName("image")
    public String image;
    public int type;
    public boolean isCompleted;
    public boolean isSendTodo;
}
