package br.com.jera.retro.api;

import java.util.ArrayList;

import br.com.jera.retro.BuildConfig;
import br.com.jera.retro.model.Task;
import br.com.jera.retro.model.User;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by daividsilverio on 3/7/16.
 */
public class ApiClientImpl implements ApiClient {

    private static ApiClientImpl apiClientImplSingleton;

    private ApiClientImpl() {

    }

    private APIServices apiServices;

    private APIServices getServices() {
        if (apiServices == null)
            buildServices();
        return apiServices;
    }

    private APIServices buildServices() {
        if (apiServices == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.API_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiServices = retrofit.create(APIServices.class);
        }

        return apiServices;
    }

    public static ApiClientImpl getInstance(){
        if (apiClientImplSingleton == null){
            apiClientImplSingleton = new ApiClientImpl();
        }
        return apiClientImplSingleton;
    }

    @Override
    public void login(Callback<User> callback) {
        getServices().authenticate(9 ,"lalala").enqueue(callback);
    }

    @Override
    public void getTodoTasks(Callback<ArrayList<Task>> callback) {
        getServices().getTodoList().enqueue(callback);
    }

    @Override
    public void getGoodTasks(Callback<ArrayList<Task>> callback) {
        getServices().getGoodList().enqueue(callback);
    }

    @Override
    public void getBetterTasks(Callback<ArrayList<Task>> callback) {
        getServices().getBetterList().enqueue(callback);
    }
}
