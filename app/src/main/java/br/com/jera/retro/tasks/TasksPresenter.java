package br.com.jera.retro.tasks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import br.com.jera.retro.api.ApiClient;
import br.com.jera.retro.model.Task;
import br.com.jera.retro.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by atomicavocado on 2/24/16.
 */
public class TasksPresenter implements TasksContract.Presenter {

    @NonNull
    private final TasksContract.View viewInteraction;

    @NonNull
    private final ApiClient apiClient;

    public TasksPresenter(@NonNull TasksContract.View viewInteraction, ApiClient apiClient) {
        this.viewInteraction = viewInteraction;
        this.apiClient = apiClient;
    }

    @Override
    public void getTasks(int taskType) {
        viewInteraction.showLoadingView();
        switch (taskType) {
            case Constants.TODO_TASK:
                getTodoList();
                break;
            case Constants.GOOD_TASK:
                getGoodList();
                break;
            case Constants.BETTER_TASK:
                getBetterList();
                break;
            default:
                break;
        }

    }

    private void getTodoList() {
        apiClient.getTodoTasks(new Callback<ArrayList<Task>>() {
            @Override
            public void onResponse(Call<ArrayList<Task>> call, Response<ArrayList<Task>> response) {
                viewInteraction.showTasks(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<Task>> call, Throwable t) {
                viewInteraction.showTasksError(t.getMessage());
            }
        });
    }

    private void getGoodList() {
        apiClient.getGoodTasks(new Callback<ArrayList<Task>>() {
            @Override
            public void onResponse(Call<ArrayList<Task>> call, Response<ArrayList<Task>> response) {
                viewInteraction.showTasks(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<Task>> call, Throwable t) {
                viewInteraction.showTasksError(t.getMessage());

            }
        });
    }

    private void getBetterList() {
        apiClient.getBetterTasks(new Callback<ArrayList<Task>>() {
            @Override
            public void onResponse(Call<ArrayList<Task>> call, Response<ArrayList<Task>> response) {
                viewInteraction.showTasks(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<Task>> call, Throwable t) {
                viewInteraction.showTasksError(t.getMessage());
            }
        });
    }
}