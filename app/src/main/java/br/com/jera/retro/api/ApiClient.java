package br.com.jera.retro.api;

import java.util.ArrayList;

import br.com.jera.retro.model.Task;
import br.com.jera.retro.model.User;
import retrofit2.Callback;

/**
 * Created by vitormesquita on 15/03/16.
 */
public interface ApiClient {

    void login(Callback<User> callback);

    void getTodoTasks(Callback<ArrayList<Task>> callback);

    void getGoodTasks(Callback<ArrayList<Task>> callback);

    void getBetterTasks(Callback<ArrayList<Task>> callback);
}
