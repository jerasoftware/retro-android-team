package br.com.jera.retro.utils;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.jera.retro.RetroApplication;

/**
 * Created by A.Scalabrini on 3/9/16.
 */
public class StaticPreferences implements Preferences {

    private static Preferences instance;

    public static Preferences getInstance() {
        if (instance == null) {
            instance = new StaticPreferences();
        }
        return instance;
    }

    @Override
    public SharedPreferences getPrefs() {
        return RetroApplication.getContext().getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public void setCurrentUser(String user) {
        getPrefs().edit().putString(Constants.CURRENT_USER, user).commit();
    }

    @Override
    public String getCurrentUser() {
        return getPrefs().getString(Constants.CURRENT_USER, null);
    }
}
