package br.com.jera.retro.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import br.com.jera.retro.R;
import br.com.jera.retro.model.TaskTypeEnum;
import butterknife.OnClick;

/**
 * Created by analirascalabrini on 27/04/16.
 */
public class FabClickListenerHelper implements View.OnClickListener {
    private final TaskTypeEnum typeEnum;
    private final Context context;
    private EditText editTextTask;

    public FabClickListenerHelper(TaskTypeEnum typeEnum, Context context) {
        this.typeEnum = typeEnum;
        this.context = context;
    }

    @Override
    public void onClick(View view) {
        showEditTextDialog(context);
    }

    private void showEditTextDialog(Context context) {
        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title(typeEnum == TaskTypeEnum.IMPROVE ? context.getString(R.string.dialog_title_improve) : context.getString(R.string.dialog_title_good))
                .customView(R.layout.dialog_custom_view, true)
                .positiveText(R.string.global_send)
                .negativeText(R.string.global_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.d("retro", editTextTask.getText().toString());
                        //TODO enviar texto
                    }
                }).build();
        editTextTask = (EditText) dialog.getCustomView().findViewById(R.id.edittext_description);
        dialog.show();
    }
}
