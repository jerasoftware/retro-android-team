package br.com.jera.retro.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import br.com.jera.retro.weekFace.WeekFaceActivity;

/**
 * Created by analirascalabrini on 20/04/16.
 */
public class MainPresenter implements MainContract.Presenter {

    @NonNull
    private final MainContract.View viewInteraction;

    public MainPresenter(@NonNull MainContract.View viewInteraction) {
        this.viewInteraction = viewInteraction;
    }

    @Override
    public void menuWeekFace(Context context) {
        context.startActivity(new Intent(context, WeekFaceActivity.class));
    }

    @Override
    public void menuLogout() {
        //TODO
    }
}
