package br.com.jera.retro.api;

import java.util.ArrayList;

import br.com.jera.retro.model.Task;
import br.com.jera.retro.model.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by vitormesquita on 08/03/16.
 */
public interface APIServices {

    @FormUrlEncoded
    @POST("api/authenticate")
    Call<User> authenticate(@Field("user_id") long userId, @Field("password") String password);

    @GET("api/todo_list")
    Call<ArrayList<Task>> getTodoList();

    @GET("api/good_list")
    Call<ArrayList<Task>> getGoodList();

    @GET("api/better_list")
    Call<ArrayList<Task>> getBetterList();
}