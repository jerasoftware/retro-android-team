package br.com.jera.retro.utils;

import android.content.SharedPreferences;

/**
 * Created by daividsilverio on 4/18/16.
 */
public interface Preferences {
    SharedPreferences getPrefs();

    void setCurrentUser(String user);

    String getCurrentUser();
}
