package br.com.jera.retro.tasks;

import java.util.ArrayList;

import br.com.jera.retro.model.Task;

/**
 * Created by atomicavocado on 2/24/16.
 */
public interface TasksContract {

    interface View {
        void showTasks(ArrayList<Task> taskList);

        void showTasksError(String messageError);

        void showEmptyView();

        void showLoadingView();
    }

    interface Presenter {
        void getTasks(int taskType);
    }
}
