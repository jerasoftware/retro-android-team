package br.com.jera.retro.weekFace;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import br.com.jera.retro.R;
import br.com.jera.retro.base.BaseActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by analirascalabrini on 20/04/16.
 */
public class WeekFaceActivity extends BaseActivity implements WeekFaceContract.View {

    @Bind(R.id.recyclerview)
    RecyclerView recyclerViewTasks;

    private WeekFacePresenter weekFacePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_face);
        ButterKnife.bind(this);
        weekFacePresenter = new WeekFacePresenter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_posts:
                weekFacePresenter.menuPosts(this);
                return true;
            case R.id.menu_week_face:
                return true;
            case R.id.mmenu_logout:
                weekFacePresenter.menuLogout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
