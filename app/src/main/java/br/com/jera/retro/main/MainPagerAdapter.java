package br.com.jera.retro.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.jera.retro.tasks.TasksFragment;
import br.com.jera.retro.utils.Constants;

/**
 * Created by vitormesquita on 26/02/16.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return TasksFragment.newInstance(Constants.TODO_TASK);
            case 1:
                return TasksFragment.newInstance(Constants.GOOD_TASK);
            case 2:
                return TasksFragment.newInstance(Constants.BETTER_TASK);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
