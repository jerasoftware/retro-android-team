package br.com.jera.retro.main;

import android.content.Context;
import android.content.Intent;

/**
 * Created by analirascalabrini on 20/04/16.
 */
public interface MainContract {

    interface View {

    }

    interface Presenter {
        void menuWeekFace(Context context);
        void menuLogout();
    }
}
