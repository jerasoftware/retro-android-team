package br.com.jera.retro.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import br.com.jera.retro.R;
import br.com.jera.retro.base.BaseActivity;
import br.com.jera.retro.model.TaskTypeEnum;
import br.com.jera.retro.utils.FabClickListenerHelper;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainContract.View {

    @Bind(R.id.toolbar)
    public Toolbar toolbar;
    @Bind(R.id.tab_layout_main)
    public TabLayout tabLayout;
    @Bind(R.id.view_pager_main)
    public ViewPager viewPager;
    @Bind(R.id.fab_button)
    FloatingActionsMenu floatingActionsMenu;

    private MainPresenter mainPresenter;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenter = new MainPresenter(this);
        setToolbarToActionBar(toolbar);

        implementFloatingActionMenu();

        tabLayout.addTab(tabLayout.newTab().setText(R.string.todo_title_uppercase));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.good_title_uppercase));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.improve_title_uppercase));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        MainPagerAdapter pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_posts:
                return true;
            case R.id.menu_week_face:
                mainPresenter.menuWeekFace(this);
                return true;
            case R.id.mmenu_logout:
                mainPresenter.menuLogout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void implementFloatingActionMenu() {
        FloatingActionButton actionImprove = new FloatingActionButton(getBaseContext());
        actionImprove.setColorNormalResId(R.color.button_improve);
        actionImprove.setSize(FloatingActionButton.SIZE_MINI);
        actionImprove.setOnClickListener( new FabClickListenerHelper(TaskTypeEnum.IMPROVE, this));

        FloatingActionButton actionGood = new FloatingActionButton(getBaseContext());
        actionGood.setColorNormalResId(R.color.button_good);
        actionGood.setSize(FloatingActionButton.SIZE_MINI);
        actionGood.setOnClickListener(new FabClickListenerHelper(TaskTypeEnum.GOOD, this));

        floatingActionsMenu.addButton(actionImprove);
        floatingActionsMenu.addButton(actionGood);
    }

}
