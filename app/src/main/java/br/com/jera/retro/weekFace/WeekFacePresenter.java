package br.com.jera.retro.weekFace;

import android.content.Context;
import android.support.annotation.NonNull;

import br.com.jera.retro.main.MainContract;

/**
 * Created by analirascalabrini on 20/04/16.
 */
public class WeekFacePresenter implements WeekFaceContract.Presenter{
    @NonNull
    private final WeekFaceContract.View viewInteraction;

    public WeekFacePresenter(@NonNull WeekFaceContract.View viewInteraction) {
        this.viewInteraction = viewInteraction;
    }

    @Override
    public void menuPosts(WeekFaceActivity activity) {
        activity.onBackPressed();
    }

    @Override
    public void menuLogout() {
        //TODO
    }
}
