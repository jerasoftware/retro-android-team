package br.com.jera.retro.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import br.com.jera.retro.utils.StaticPreferences;

/**
 * Created by daividsilverio on 2/22/16.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 4510844415963088259L;
    private static User currentUser;

    @SerializedName("email")
    public String email;
    @SerializedName("name")
    public String name;
    @SerializedName("avatar_url")
    public String avatarUrl;
    @SerializedName("votes")
    public int votes;

    public static User getCurrent() {
        if (currentUser == null) {
            String json = StaticPreferences.getInstance().getCurrentUser();
            currentUser = new Gson().fromJson(json, User.class);
        }
        return currentUser;
    }

    public static void setCurrent(User currentUser) {
        StaticPreferences.getInstance().setCurrentUser(new Gson().toJson(currentUser));
        User.currentUser = currentUser;
    }

}
