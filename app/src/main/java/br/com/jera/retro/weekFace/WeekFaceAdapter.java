package br.com.jera.retro.weekFace;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.jera.retro.R;
import br.com.jera.retro.model.User;
import butterknife.Bind;

/**
 * Created by nestor on 04/05/16.
 */
public class WeekFaceAdapter extends RecyclerView.Adapter<WeekFaceAdapter.WeekFaceViewHolder> {

    private Context context;
    private List<User> users;
    private final WeekFaceAdapterListener listener;

    public WeekFaceAdapter(Context context, List<User> users, WeekFaceAdapterListener listener) {
        this.context = context;
        this.users = users;
        this.listener = listener;
    }

    @Override
    public WeekFaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_week_face, parent, false);
        return new WeekFaceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WeekFaceViewHolder holder, int position) {
        final User user = users.get(position);
        holder.format(user);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class WeekFaceViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.linear_layout_container)
        LinearLayout containerLinearLayout;
        @Bind(R.id.text_view_user_name)
        TextView textViewUserName;
        @Bind(R.id.image_view_user)
        ImageView userImageView;
        @Bind(R.id.check_box_vote)
        CheckBox checkBoxVote;

        public WeekFaceViewHolder(View itemView) {
            super(itemView);
        }

        private void format(final User user) {
            if (user.avatarUrl != null) {
                Picasso.with(context).load(user.avatarUrl).into(userImageView);
            } else {
                userImageView.setImageResource(R.drawable.ic_avatar_placeholder);
            }
            textViewUserName.setText(user.name);
            checkBoxVote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (listener != null) {
                        listener.onClickVoteListener(user);
                    }
                }
            });
        }
    }

    public interface WeekFaceAdapterListener {
        void onClickVoteListener(User user);
    }
}
