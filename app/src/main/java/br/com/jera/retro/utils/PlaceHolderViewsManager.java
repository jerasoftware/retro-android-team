package br.com.jera.retro.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

/**
 * Created by vitormesquita on 22/03/16.
 */
public class PlaceHolderViewsManager {

    private ViewStub loadingViewStub, emptyViewStub, errorViewStub;
    private int errorMessageTextViewId, errorTryAgainViewId, errorLoginViewId, emptyActionViewId;
    private View errorView, emptyView, loadingView, errorTryAgainView, errorLoginView, emptyActionView;
    private TextView errorMessageTextView;

    private PlaceHolderViewsManager() {
        // nothing to do here
    }

    private void setLoadingViewStub(ViewStub loadingViewStub) {
        this.loadingViewStub = loadingViewStub;
    }

    private void setEmptyViewStub(ViewStub emptyViewStub, int actionViewId){
        this.emptyViewStub = emptyViewStub;
        this.emptyActionViewId = actionViewId;
    }

    private void setErrorViewStub(ViewStub errorViewStub, int messageTextViewId, int tryAgainViewId) {
        this.errorViewStub = errorViewStub;
        this.errorMessageTextViewId = messageTextViewId;
        this.errorTryAgainViewId = tryAgainViewId;
    }

    public void showLoading() {
        validateLoadingView();
        hideAll();
        setViewVisibility(loadingView, View.VISIBLE);
    }

    public void showEmpty() {
        validateEmptyView();
        hideAll();
        setViewVisibility(emptyView, View.VISIBLE);
    }

    public void showEmpty(int messageStringId) {
        showEmpty();
        ((TextView) emptyView).setText(messageStringId);
    }

    public void showEmpty(View.OnClickListener actionCallback) {
        showEmpty();
        emptyActionView.setOnClickListener(actionCallback);
    }

    public void showEmpty(int messageStringId, View.OnClickListener actionCallback) {
        showEmpty(messageStringId);
        emptyActionView.setOnClickListener(actionCallback);
    }

    public void showError() {
        validateErrorView();
        hideAll();
        setViewVisibility(errorView, View.VISIBLE);
    }

    public void showError(Throwable error, View.OnClickListener tryAgainCallback) {
        if (errorView == null) {
            validateErrorView();
        }
        Context context = errorView.getContext();
        showError(ApplicationUtils.getMessage(context, error), tryAgainCallback);
    }

    public void showError(String message, View.OnClickListener tryAgainCallback) {
        showError();
        errorMessageTextView.setText(message);
        errorTryAgainView.setOnClickListener(tryAgainCallback);
    }


    public void hideAll() {
        hideLoading();
        hideEmpty();
        hideError();
    }

    public void hideLoading() {
        setViewVisibility(loadingView, View.GONE);
    }

    public void hideError() {
        setViewVisibility(errorView, View.GONE);
    }

    public void hideEmpty() {
        setViewVisibility(emptyView, View.GONE);
    }

    private void validateLoadingView() {
        if (loadingView == null) {
            loadingView = inflateStubOrBust(loadingViewStub, "loadingView");
        }
    }

    private void validateEmptyView(){
        if (emptyView == null){
            emptyView = inflateStubOrBust(emptyViewStub, "emptyView");
            if (emptyActionViewId != 0) {
                emptyActionView = findChildOrBust(emptyView, emptyActionViewId, "emptyActionView");
            }
        }
    }

    private void validateErrorView() {
        if (errorView == null) {
            errorView = inflateStubOrBust(errorViewStub, "errorView");
            errorMessageTextView = (TextView) findChildOrBust(errorView, errorMessageTextViewId, "errorMessageTextView");
            errorTryAgainView = findChildOrBust(errorView, errorTryAgainViewId, "errorTryAgainView");
        }
    }


    private void setViewVisibility(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private View inflateStubOrBust(ViewStub viewStub, String viewName) {
        if (viewStub == null) {
            throw new RuntimeException(viewName + " precisa ser inflada mas seu stub nao foi adicionado ao construtor!");
        }
        return viewStub.inflate();
    }

    private View findChildOrBust(View view, int childViewId, String childViewName) {
        View childView = view.findViewById(childViewId);
        if (childView == null && childViewId != 0) {
            throw new RuntimeException("Id da view " + childViewName + " foi adicionada ao construtor mas nao foi encontrada!");
        }
        return childView;
    }

    public static Builder builder(View view) {
        return new Builder(view);
    }

    public static Builder builder(Activity activity) {
        return new Builder(activity);
    }


    public static class Builder{

        private View view;
        private PlaceHolderViewsManager manager;

        public Builder(Activity activity){
            this(activity.getWindow().getDecorView());
        }

        public Builder(View view) {
            this.view = view;
            this.manager = new PlaceHolderViewsManager();
        }

        public Builder loadingStub(int loadingViewStub){
            manager.setLoadingViewStub(findStubOrBust(loadingViewStub, "loadingViewStub"));
            return this;
        }

        public Builder emptyStub(int emptyViewStubId, int actionViewId) {
            manager.setEmptyViewStub(findStubOrBust(emptyViewStubId, "emptyViewStub"), actionViewId);
            return this;
        }

        public Builder emptyStub(int emptyViewStubId){
            return emptyStub(emptyViewStubId, 0);
        }

        public Builder errorStub(int errorViewStubId, int messageTextViewId, int tryAgainViewId) {
            manager.setErrorViewStub(findStubOrBust(errorViewStubId, "errorViewStub"), messageTextViewId, tryAgainViewId);
            return this;
        }

        public Builder errorStub(int errorViewStubId) {
            return errorStub(errorViewStubId, 0, 0);
        }

        public Builder errorStub(int errorViewStubId, int messageTextViewId) {
            return errorStub(errorViewStubId, messageTextViewId, 0);
        }

        public PlaceHolderViewsManager build(){
            return manager;
        }

        private ViewStub findStubOrBust(int childViewId, String childViewName) {
            View childView = view.findViewById(childViewId);
            if (childView == null) {
                throw new RuntimeException("ViewStub " + childViewName + " nao encontrado!");
            }
            return (ViewStub) childView;
        }
    }
}
