package br.com.jera.retro.weekFace;

import android.content.Context;

/**
 * Created by analirascalabrini on 20/04/16.
 */
public interface WeekFaceContract {

    interface View {

    }

    interface Presenter {
        void menuPosts(WeekFaceActivity activity);
        void menuLogout();
    }
}
