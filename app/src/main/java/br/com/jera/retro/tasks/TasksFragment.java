package br.com.jera.retro.tasks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.jera.retro.R;
import br.com.jera.retro.api.ApiClientImpl;
import br.com.jera.retro.base.BaseFragment;
import br.com.jera.retro.model.Task;
import br.com.jera.retro.utils.Constants;
import br.com.jera.retro.utils.PlaceHolderViewsManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by atomicavocado on 2/24/16.
 */
public class TasksFragment extends BaseFragment implements TasksContract.View, TasksAdapter.TaskAdapterCheckBoxListener, TasksAdapter.TaskAdapterCheckBoxStarListener {

    @Bind(R.id.recyclerview)
    RecyclerView recyclerViewTasks;

    private TasksPresenter tasksPresenter;
    private PlaceHolderViewsManager placeHolderViewsManager;
    private int taskType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);
        ButterKnife.bind(this, view);

        taskType = getArguments().getInt(Constants.TASK_TYPE);

        recyclerViewTasks.setLayoutManager(new LinearLayoutManager(getContext()));
        placeHolderViewsManager = PlaceHolderViewsManager.builder(view).loadingStub(R.id.stub_loading).build();

        tasksPresenter = new TasksPresenter(this, ApiClientImpl.getInstance());
        tasksPresenter.getTasks(taskType);
        return view;
    }

    @Override
    public void showTasks(ArrayList<Task> taskList) {
        placeHolderViewsManager.hideLoading();
        if (taskList != null && taskList.size() > 0) {
            recyclerViewTasks.setAdapter(new TasksAdapter(taskList, this, getContext(), taskType, this));
        }
    }

    @Override
    public void showTasksError(String messageError) {
        //TODO
    }

    @Override
    public void showEmptyView() {
        //TODO
    }

    @Override
    public void showLoadingView() {
        placeHolderViewsManager.showLoading();
    }

    @Override
    public void onClickListener(Task task){
        if (taskType == Constants.TODO_TASK){
            task.isCompleted = !task.isCompleted;
        }else{
            task.isSendTodo = !task.isSendTodo;
        }
    }

    public static TasksFragment newInstance(int taskType) {
        Bundle args = new Bundle();
        TasksFragment fragment = new TasksFragment();
        args.putInt(Constants.TASK_TYPE, taskType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClickStarListener(Task task) {
        Toast.makeText(getActivity(), R.string.fragment_tasks_post_send_todo, Toast.LENGTH_SHORT).show();
    }
}