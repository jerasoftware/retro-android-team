package br.com.jera.retro.utils;

import android.content.Context;

/**
 * Created by vitormesquita on 22/03/16.
 */
public class ApplicationUtils {

    public static String getMessage(Context context, Throwable error) {
        return error.getLocalizedMessage();
    }
}
