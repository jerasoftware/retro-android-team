package br.com.jera.retro.tasks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.jera.retro.R;
import br.com.jera.retro.model.Task;
import br.com.jera.retro.utils.Constants;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by atomicavocado on 2/24/16.
 */
public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    private final ArrayList<Task> taskList;
    private final TaskAdapterCheckBoxListener listener;
    private final TaskAdapterCheckBoxStarListener listenerStar;
    private Context context;
    private int taskType;

    public TasksAdapter(ArrayList<Task> taskList, TaskAdapterCheckBoxListener listener, Context context, int taskType, TaskAdapterCheckBoxStarListener listenerStar) {
        this.taskList = taskList;
        this.listener = listener;
        this.context = context;
        this.taskType = taskType;
        this.listenerStar = listenerStar;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_task, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Task task = getItem(position);
        holder.format(task);
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    private Task getItem(int position) {
        return position >= 0 && position < getItemCount() ? taskList.get(position) : null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.linear_layout_container)
        LinearLayout containerLinearLayout;
        @Bind(R.id.text_view_sender_name)
        TextView textViewSenderName;
        @Bind(R.id.text_view_description)
        TextView textViewDescription;
        @Bind(R.id.check_box)
        CheckBox checkBox;
        @Bind(R.id.image_view_user)
        ImageView userImageView;
        @Bind(R.id.check_box_star)
        CheckBox checkBoxStar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void format(final Task task) {
            textViewSenderName.setText(task.sender);
            textViewDescription.setText(task.description);
            if (task.image != null) {
                Picasso.with(context).load(task.image).into(userImageView);
            }
            checkBox.setVisibility(View.VISIBLE);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (listener != null) {
                        listener.onClickListener(task);
                        task.isCompleted = isChecked;
                    }
                }
            });
            checkBox.setChecked(task.isCompleted);
            checkBoxStar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        listenerStar.onClickStarListener(task);
                    }
                }
            });

            switch (taskType) {
                case Constants.TODO_TASK:
                    checkBox.setChecked(task.isCompleted);
                    break;
                case Constants.BETTER_TASK:
//                    checkBox.setChecked(task.isSendTodo);
                    checkBox.setVisibility(View.GONE);
                    checkBoxStar.setVisibility(View.VISIBLE);
                    break;
                default:
                    checkBox.setVisibility(View.GONE);
                    break;
            }
        }
    }

    public interface TaskAdapterCheckBoxListener {
        void onClickListener(Task task);
    }

    public interface TaskAdapterCheckBoxStarListener {
        void onClickStarListener(Task task);
    }
}