package br.com.jera.retro.login;

import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;

import br.com.jera.retro.main.MainActivity;
import br.com.jera.retro.R;
import br.com.jera.retro.api.ApiClientImpl;
import br.com.jera.retro.base.BaseActivity;
import br.com.jera.retro.model.User;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginContract.View {

    private LoginPresenter loginPresenter;

    private GoogleApiClient googleApiClient;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this, ApiClientImpl.getInstance());

        if (User.getCurrent() != null) {
            openMainActivity();
        } else {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail().build();
            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, loginPresenter)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginPresenter.onGoogleLoginActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.button_login_google)
    public void loginGoogle() {
        loginPresenter.openLogin();
    }

    @Override
    public void openLoginClicked() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, LoginPresenter.RC_SIGN_IN);
    }

    @Override
    public void showLoginErrorDialog(String title, String errorMessage) {
        new MaterialDialog.Builder(this)
                .title(title)
                .content(errorMessage)
                .positiveText(R.string.global_try_gain)
                .negativeText(R.string.global_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        loginGoogle();
                    }
                }).show();
    }

    @Override
    public void showProgressBar() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(R.string.global_wait)
                .progress(true, 0).show();
    }

    @Override
    public void hideProgressBar() {
        materialDialog.hide();
    }

    @Override
    public void openMainActivity() {
        MainActivity.start(this);
    }
}
