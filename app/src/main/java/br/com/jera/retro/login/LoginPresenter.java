package br.com.jera.retro.login;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import br.com.jera.retro.R;
import br.com.jera.retro.api.ApiClient;
import br.com.jera.retro.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vitormesquita on 19/02/16.
 */
public class LoginPresenter implements LoginContract.Presenter, GoogleApiClient.OnConnectionFailedListener {

    public static final int RC_SIGN_IN = 1111;

    @NonNull
    private final LoginContract.View viewInteraction;

    @NonNull
    private final ApiClient apiClient;

    public LoginPresenter(@NonNull LoginContract.View viewInteraction, @NonNull ApiClient apiClient) {
        this.viewInteraction = viewInteraction;
        this.apiClient = apiClient;
    }

    @Override
    public void openLogin() {
        viewInteraction.openLoginClicked();
    }

    @Override
    public void onGoogleLoginActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result == null || !result.isSuccess()) {
                onLoginFailure(viewInteraction.getString(R.string.error_connection_failed));
            } else {
                makeLoginRequest();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        onLoginFailure(R.string.error_connection_failed_and_why, connectionResult.getErrorMessage());
    }

    public void makeLoginRequest() {
        viewInteraction.showProgressBar();
        apiClient.login(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                viewInteraction.hideProgressBar();
                onLoginSuccess(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                viewInteraction.hideProgressBar();
                viewInteraction.showLoginErrorDialog("lalalala", "error"); //TODO Argumentos tem que ser iguais ao teste!
            }
        });
    }

    public void onLoginSuccess(User user) {
        User.setCurrent(user);
        this.viewInteraction.openMainActivity();
    }

    private void onLoginFailure(String errorMessage) {
        String errorTitle = viewInteraction.getString(R.string.activity_home_screen_authentication_failed_title);
        this.viewInteraction.showLoginErrorDialog(errorTitle, errorMessage);
    }

    public void onLoginFailure() {
        String errorMessage = viewInteraction.getString(R.string.error_connection_failed);
        String errorTitle = viewInteraction.getString(R.string.activity_home_screen_authentication_failed_title);
        this.viewInteraction.showLoginErrorDialog(errorTitle, errorMessage);
    }

    private void onLoginFailure(int baseFormattedErrorStringIdWithDetails, String errorDetail) {
        String errorMessage = viewInteraction.getString(baseFormattedErrorStringIdWithDetails, errorDetail);
        String errorTitle = viewInteraction.getString(R.string.activity_home_screen_authentication_failed_title);
        this.viewInteraction.showLoginErrorDialog(errorTitle, errorMessage);
    }
}