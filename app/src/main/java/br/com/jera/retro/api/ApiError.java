package br.com.jera.retro.api;

/**
 * Created by daividsilverio on 3/7/16.
 */
public interface ApiError {
    String getErrorMessage();
    ErrorType getErrorType();

    enum ErrorType {
        API,
        NETWORK,
        CONVERSION
    }
}
