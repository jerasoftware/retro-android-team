package br.com.jera.retro.model;

/**
 * Created by analirascalabrini on 27/04/16.
 */
public enum TaskTypeEnum {
    GOOD, IMPROVE
}
