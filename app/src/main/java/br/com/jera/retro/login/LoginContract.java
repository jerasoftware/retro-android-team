package br.com.jera.retro.login;

import android.content.Intent;

/**
 * Created by vitormesquita on 19/02/16.
 */
public interface LoginContract {

    interface View {
        void openLoginClicked();

        void showLoginErrorDialog(String title, String errorMessage);

        void showProgressBar();

        void hideProgressBar();

        void openMainActivity();

        String getString(int stringId);

        String getString(int stringId, Object... args);
    }

    interface Presenter {
        void openLogin();

        void onGoogleLoginActivityResult(int requestCode, int resultCode, Intent data);
    }
}
