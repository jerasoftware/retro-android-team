package br.com.jera.retro.utils;

/**
 * Created by A.Scalabrini on 3/9/16.
 */
public class Constants {

    public static final String PREFERENCES = "preferences";
    public static final String CURRENT_USER = "current user";
    public static final String TASK_TYPE = "task_type";
    public static final int TODO_TASK = 17;
    public static final int GOOD_TASK = 18;
    public static final int BETTER_TASK = 19;
}
