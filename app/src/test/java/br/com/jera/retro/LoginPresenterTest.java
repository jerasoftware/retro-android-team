package br.com.jera.retro;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.jera.retro.api.ApiClient;
import br.com.jera.retro.login.LoginContract;
import br.com.jera.retro.login.LoginPresenter;
import br.com.jera.retro.model.User;
import br.com.jera.retro.utils.Preferences;
import br.com.jera.retro.utils.StaticPreferences;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vitormesquita on 19/02/16.
 * <p/>
 * FAZENDO TESTES UNITARIOS PARA O PRESENTER
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(StaticPreferences.class)
public class LoginPresenterTest {

    @Mock
    private LoginContract.View homeScreenContractView;

    @Mock
    private ApiClient apiClient;

    @Mock
    private Preferences preferences;

    private User user;

    @Captor
    private ArgumentCaptor<Callback<User>> callbackCaptor;

    @Captor
    private ArgumentCaptor<User> preferencesCaptor;

    private LoginPresenter presenter;

    @Before
    public void setupHomeScreenPresenter() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(StaticPreferences.class);
        user = new User();
        user.name = "John Snow";
        user.email = "johnsnow@jera.com.br";
        user.avatarUrl = "https://robohash.org/bgset_bg2/johnsnow_";
        presenter = new LoginPresenter(homeScreenContractView, apiClient);
    }

    @Test
    public void presenter_login_success(){
        presenter.makeLoginRequest();
        verify(homeScreenContractView).showProgressBar();
        verify(apiClient).login(callbackCaptor.capture());
        when(StaticPreferences.getInstance()).thenReturn(preferences);
        callbackCaptor.getValue().onResponse(null, Response.success(user));
        verify(homeScreenContractView).hideProgressBar();
        verify(homeScreenContractView).openMainActivity();
    }

    @Test
    public void presenter_login_failure() {
        presenter.makeLoginRequest();
        verify(homeScreenContractView).showProgressBar();
        verify(apiClient).login(callbackCaptor.capture());
        callbackCaptor.getValue().onFailure(null, new Throwable());
        verify(homeScreenContractView).hideProgressBar();
        verify(homeScreenContractView).showLoginErrorDialog("lalalala", "error");
    }
}
