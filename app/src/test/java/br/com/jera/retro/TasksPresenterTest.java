package br.com.jera.retro;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import br.com.jera.retro.api.ApiClient;
import br.com.jera.retro.model.Task;
import br.com.jera.retro.tasks.TasksContract;
import br.com.jera.retro.tasks.TasksPresenter;
import br.com.jera.retro.utils.Constants;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by atomicavocado on 2/24/16.
 */
public class TasksPresenterTest {

    private ArrayList<Task> arrayListTasks;

    @Mock
    private TasksContract.View tasksScreenContractView;

    @Mock
    private ApiClient apiClient;

    @Captor
    private ArgumentCaptor<Callback<ArrayList<Task>>> callbackCaptor;

    @Mock
    private Context context;

    private TasksPresenter tasksPresenter;
    private Throwable t;

    @Before
    public void setupTasksScreenPresenter() {
        MockitoAnnotations.initMocks(this);
        tasksPresenter = new TasksPresenter(tasksScreenContractView, apiClient);
        arrayListTasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.sender = "Mano";
            task.description = "lalalala";
            arrayListTasks.add(task);
        }
        t = new Throwable("Mensagem de erro");
    }

    @Test
    public void presenter_show_todo_tasks() {
        tasksPresenter.getTasks(Constants.TODO_TASK);
        verify(apiClient).getTodoTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onResponse(null, Response.success(arrayListTasks));
        verify(tasksScreenContractView).showTasks(arrayListTasks);
    }

    @Test
    public void presenter_show_todo_task_error() {
        tasksPresenter.getTasks(Constants.TODO_TASK);
        verify(apiClient).getTodoTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onFailure(null, t);
        verify(tasksScreenContractView).showTasksError(t.getMessage());
    }

    @Test
    public void presenter_show_good_tasks() {
        tasksPresenter.getTasks(Constants.GOOD_TASK);
        verify(tasksScreenContractView).showLoadingView();
        verify(apiClient).getGoodTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onResponse(null, Response.success(arrayListTasks));
        verify(tasksScreenContractView).showTasks(arrayListTasks);
    }

    @Test
    public void presenter_show_good_task_error() {
        tasksPresenter.getTasks(Constants.GOOD_TASK);
        verify(apiClient).getGoodTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onFailure(null, t);
        verify(tasksScreenContractView).showTasksError(t.getMessage());
    }

    @Test
    public void presenter_show_better_tasks() {
        tasksPresenter.getTasks(Constants.BETTER_TASK);
        verify(tasksScreenContractView).showLoadingView();
        verify(apiClient).getBetterTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onResponse(null, Response.success(arrayListTasks));
        verify(tasksScreenContractView).showTasks(arrayListTasks);
    }

    @Test
    public void presenter_show_better_tasks_error() {
        tasksPresenter.getTasks(Constants.BETTER_TASK);
        verify(apiClient).getBetterTasks(callbackCaptor.capture());
        callbackCaptor.getValue().onFailure(null, t);
        verify(tasksScreenContractView).showTasksError(t.getMessage());
    }
}
